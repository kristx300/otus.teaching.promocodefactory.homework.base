﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Otus.Teaching.PromoCodeFactory.Utils.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : IBaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [MaxLength(255,ErrorMessage = "Максимальная длина имени равна 255 символам")]
        public string FirstName { get; set; }
        [MaxLength(255, ErrorMessage = "Максимальная длина фамилии равна 255 символам")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual ICollection<RoleEmployee> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}