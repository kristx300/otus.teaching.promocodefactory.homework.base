﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class RoleEmployee
    {
        [Key]
        [ForeignKey(nameof(RoleId))]
        public virtual Role Role { get; set; }
        public long RoleId { get; set; }

        [Key]
        [ForeignKey(nameof(EmployeeId))]
        public virtual Employee Employee { get; set; }
        public long EmployeeId { get; set; }

        public RoleEmployee()
        {
            
        }

        public RoleEmployee(Role r, Employee e)
        {
            Role = r;
            RoleId = r.Id;

            Employee = e;
            EmployeeId = e.Id;
        }
    }
}
