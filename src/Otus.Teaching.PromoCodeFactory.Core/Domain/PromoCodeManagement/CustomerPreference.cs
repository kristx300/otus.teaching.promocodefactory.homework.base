﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
    {
        [Key]
        [ForeignKey(nameof(CustomerId))]
        public virtual Customer Customer { get; set; }
        public long CustomerId { get; set; }

        [Key]
        [ForeignKey(nameof(PreferenceId))]
        public virtual Preference Preference { get; set; }
        public long PreferenceId { get; set; }
    }
}
