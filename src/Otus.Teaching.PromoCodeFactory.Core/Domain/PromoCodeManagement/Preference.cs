﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Otus.Teaching.PromoCodeFactory.Utils.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference : IBaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [MaxLength(255, ErrorMessage = "Максимальная длина названия равна 255 символам")]
        public string Name { get; set; }

        public virtual ICollection<CustomerPreference> Customers { get; set; }
    }
}