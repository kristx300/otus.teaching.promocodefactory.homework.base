﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Utils.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Base
{
    public interface IRepository<TEntity>
        where TEntity : class, IBaseModel
    {
        Task<TEntity> GetByIdOrNullAsync(long id);

        Task<TEntity> GetByIdOrFailAsync(long id);

        Task<bool> HasEntityAsync(long id);

        Task<IReadOnlyCollection<TEntity>> GetAllAsync();

        Task<long> InsertAsync(TEntity entity);

        Task UpdateAsync(TEntity data);
        Task DeleteAsync(long id);
        Task DeleteAsync(TEntity entity);

        /// <summary>
        /// Returns simple model without additional inclusions.
        /// </summary>
        /// <param name="id">Entity id.</param>
        /// <returns>Simple entity.</returns>
        Task<TEntity> SimpleOrFailAsync(long id);
    }
}
