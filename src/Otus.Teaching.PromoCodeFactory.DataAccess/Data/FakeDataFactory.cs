﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = 1,
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                //Roles = new List<Role>()
                //{
                //    Roles.FirstOrDefault(x => x.Name == "Admin")  
                //},
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = 2,
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                //Roles = new List<Role>()
                //{
                //    Roles.FirstOrDefault(x => x.Name == "PartnerManager")  
                //},
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = 1,
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = 2,
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
    }
}