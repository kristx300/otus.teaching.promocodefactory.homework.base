﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Utils.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Extensions
{
    public static class ContextExtensions
    {
        public static Task<T> ByIdOrNullAsync<T>(this IQueryable<T> query, long id)
            where T : class, IBaseModel
        {
            return query.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}