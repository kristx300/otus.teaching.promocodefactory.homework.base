﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Base;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<List<Customer>> GetAllByPreferenceAsync(long preferenceId);
    }
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(PromoCodeDbContext context) : base(context)
        {
        }

        public async Task<List<Customer>> GetAllByPreferenceAsync(long preferenceId)
        {
            return await this.GetAllInternal()
                .Where(f => f.Preferences.Any(p => p.PreferenceId == preferenceId))
                .ToListAsync();
        }
    }
}
