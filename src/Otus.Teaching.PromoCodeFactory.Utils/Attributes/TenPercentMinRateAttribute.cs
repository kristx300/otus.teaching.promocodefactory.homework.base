﻿using Otus.Teaching.PromoCodeFactory.Utils.MathHelpers;

namespace Otus.Teaching.PromoCodeFactory.Utils.Attributes
{
    public class TenPercentMinRateAttribute : PercentsAttribute
    {
        public TenPercentMinRateAttribute()
            : base(Percent.Ten)
        {
        }
    }
}