﻿namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IBaseModel : IHasId, IHasDates
    {
    }
}