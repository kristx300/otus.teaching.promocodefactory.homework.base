﻿namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IBaseUrls
    {
        string BaseUrl { get; }

        string ImageBaseUrl { get; }
    }
}