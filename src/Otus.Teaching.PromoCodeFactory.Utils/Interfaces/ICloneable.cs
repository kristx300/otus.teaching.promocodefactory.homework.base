﻿namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface ICloneable<out T>
    {
        T Clone();
    }
}