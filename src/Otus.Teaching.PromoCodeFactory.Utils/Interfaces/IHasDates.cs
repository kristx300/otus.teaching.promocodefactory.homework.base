﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IHasDates
    {
        DateTime CreatedAt { get; set; }

        DateTime UpdatedAt { get; set; }
    }
}