﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IHasDeletedAt
    {
        DateTimeOffset? DeletedAt { get; set; }
    }
}