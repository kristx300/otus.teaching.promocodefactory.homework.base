﻿namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IHasId
    {
        long Id { get; set; }
    }
}