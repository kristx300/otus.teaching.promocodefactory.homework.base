﻿using Otus.Teaching.PromoCodeFactory.Utils.Enums;

namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IHasStatus
    {
        Status Status { get; set; }

        bool Active { get; }
    }
}