﻿namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IHasStep
    {
        int? Step { get; set; }
    }
}