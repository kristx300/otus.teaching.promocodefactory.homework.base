﻿using Otus.Teaching.PromoCodeFactory.Utils.Dates;

namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IHasTimeRange : IHasFromToDates
    {
        TimeRange Range();
    }
}