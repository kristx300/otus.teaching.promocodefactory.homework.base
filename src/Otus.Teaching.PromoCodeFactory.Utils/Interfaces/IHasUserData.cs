﻿using Otus.Teaching.PromoCodeFactory.Utils.Enums;

namespace Otus.Teaching.PromoCodeFactory.Utils.Interfaces
{
    public interface IHasUserData
    {
        long Id { get; }

        string UserName { get; }

        string Email { get; }

        string FirstName { get; }

        string LastName { get; }

        Role Role { get; }
    }
}