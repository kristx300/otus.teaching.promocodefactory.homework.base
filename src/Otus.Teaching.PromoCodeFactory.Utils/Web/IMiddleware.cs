﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Otus.Teaching.PromoCodeFactory.Utils.Web
{
    public interface IMiddleware
    {
        Task InvokeAsync(HttpContext context);
    }
}