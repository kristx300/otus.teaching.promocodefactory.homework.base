﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.BaseDto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Покупатели
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;
        public CustomersController(ICustomerRepository customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }


        /// <summary>
        /// Получить данные всех покупателей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerDto>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersDto = _mapper.Map<List<CustomerDto>>(customers);

            return customersDto;
        }

        /// <summary>
        /// Получить данные покупателя по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerDto>> GetCustomerByIdAsync(long id)
        {
            var customer = await _customerRepository.GetByIdOrNullAsync(id);

            if (customer == null)
                return NotFound();

            var customerDto = _mapper.Map<CustomerDto>(customer);
            var preferences = customer.Preferences.Select(f => f.Preference).ToList();
            customerDto.Preferences = _mapper.Map<List<PreferenceDto>>(preferences);

            return customerDto;
        }


        /// <summary>
        /// Создать нового покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPost("")]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] CustomerDto dto)
        {
            var customer = _mapper.Map<Customer>(dto);

            var customerId = await _customerRepository.InsertAsync(customer);

            return customerId;
        }

        /// <summary>
        /// Обновить данные покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPut("")]
        public async Task<ActionResult<Guid>> UpdateCustomerAsync([FromBody] CustomerDto dto)
        {
            var customer = await _customerRepository.GetByIdOrNullAsync(dto.Id);

            if (customer == null)
                return NotFound();

            customer.FirstName = dto.FirstName;
            customer.LastName = dto.LastName;
            customer.Email = dto.Email;

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Guid>> DeleteCustomerAsync([FromRoute] long identity)
        {
            var employee = await _customerRepository.GetByIdOrNullAsync(identity);

            if (employee == null)
                return NotFound();

            await _customerRepository.DeleteAsync(identity);

            return Ok();
        }
    }
}
