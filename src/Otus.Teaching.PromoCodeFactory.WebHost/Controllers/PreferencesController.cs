﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.BaseDto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IPreferenceRepository _preferenceRepository;
        private readonly IMapper _mapper;
        public PreferencesController(IPreferenceRepository preferenceRepository, IMapper mapper)
        {
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }


        /// <summary>
        /// Получить данные всех предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceDto>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var preferencesDto = _mapper.Map<List<PreferenceDto>>(preferences);

            return preferencesDto;
        }

        /// <summary>
        /// Получить данные предпочтения по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PreferenceDto>> GetPreferenceByIdAsync(long id)
        {
            var preference = await _preferenceRepository.GetByIdOrNullAsync(id);

            if (preference == null)
                return NotFound();

            var preferenceDto = _mapper.Map<PreferenceDto>(preference);

            return preferenceDto;
        }


        /// <summary>
        /// Создать новое предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPost("")]
        public async Task<ActionResult<long>> CreatePreferenceAsync([FromBody] PreferenceDto dto)
        {
            var preference = _mapper.Map<Preference>(dto);

            var preferenceId = await _preferenceRepository.InsertAsync(preference);

            return preferenceId;
        }

        /// <summary>
        /// Обновить данные предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpPut("")]
        public async Task<ActionResult<Guid>> UpdatePreferenceAsync([FromBody] PreferenceDto dto)
        {
            var preference = await _preferenceRepository.GetByIdOrNullAsync(dto.Id);

            if (preference == null)
                return NotFound();

            preference.Name = dto.Name;

            await _preferenceRepository.UpdateAsync(preference);

            return Ok();
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Guid>> DeletePreferenceAsync([FromRoute] long identity)
        {
            var preference = await _preferenceRepository.GetByIdOrNullAsync(identity);

            if (preference == null)
                return NotFound();

            await _preferenceRepository.DeleteAsync(identity);

            return Ok();
        }
    }
}
