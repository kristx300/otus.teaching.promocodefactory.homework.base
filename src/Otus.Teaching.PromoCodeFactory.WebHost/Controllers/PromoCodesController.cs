﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.BaseDto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController : ControllerBase
    {
        private readonly IPromoCodeRepository _promoCodeRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;
        public PromoCodesController(IPromoCodeRepository promoCodeRepository, IMapper mapper, ICustomerRepository customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _mapper = mapper;
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить данные всех предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeDto>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var promoCodesDto = _mapper.Map<List<PromoCodeDto>>(promoCodes);

            return promoCodesDto;
        }

        /// <summary>
        /// Получить данные предпочтения по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PromoCodeDto>> GetPromoCodeByIdAsync(long id)
        {
            var promoCode = await _promoCodeRepository.GetByIdOrNullAsync(id);

            if (promoCode == null)
                return NotFound();

            var promoCodeDto = _mapper.Map<PromoCodeDto>(promoCode);

            return promoCodeDto;
        }


        /// <summary>
        /// Создать новое предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPost("")]
        public async Task<ActionResult<long>> CreatePromoCodeAsync([FromBody] PromoCodeDto dto)
        {
            var promoCode = _mapper.Map<PromoCode>(dto);

            var promoCodeId = await _promoCodeRepository.InsertAsync(promoCode);

            return promoCodeId;
        }

        /// <summary>
        /// Обновить данные предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpPut("")]
        public async Task<ActionResult<Guid>> UpdatePromoCodeAsync([FromBody] PromoCodeDto dto)
        {
            var promoCode = await _promoCodeRepository.GetByIdOrNullAsync(dto.Id);

            if (promoCode == null)
                return NotFound();

            promoCode.Code = dto.Code;
            promoCode.ServiceInfo = dto.ServiceInfo;
            promoCode.BeginDate = DateTime.Parse(dto.BeginDate);
            promoCode.EndDate = DateTime.Parse(dto.BeginDate);
            promoCode.PartnerName = dto.PartnerName;

            await _promoCodeRepository.UpdateAsync(promoCode);

            return Ok();
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:long}")]
        public async Task<ActionResult<Guid>> DeletePromoCodeAsync([FromRoute] long id)
        {
            var employee = await _promoCodeRepository.GetByIdOrNullAsync(id);

            if (employee == null)
                return NotFound();

            await _promoCodeRepository.DeleteAsync(id);

            return Ok();
        }

        /// <summary>
        /// Сохраняет новый промокод в базе данных и находит клиентов с переданным предпочтением и добавлять им данный промокод
        /// </summary>
        /// <returns></returns>
        [HttpPost("GivePromocodesToCustomersWithPreferenceAsync")]
        public async Task<ActionResult<long>> GivePromocodesToCustomersWithPreferenceAsync([FromBody] PromoCodeDto dto, int preferenceId)
        {
            var clients = await _customerRepository.GetAllByPreferenceAsync(preferenceId);
            foreach (var customer in clients)
            {
                var promoCode = _mapper.Map<PromoCode>(dto);

                promoCode.CustomerId = customer.Id;

                var promoCodeId = await _promoCodeRepository.InsertAsync(promoCode);
            }

            return clients.Count;
        }
    }
}
