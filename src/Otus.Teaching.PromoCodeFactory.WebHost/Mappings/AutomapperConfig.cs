﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.BaseDto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappings
{
    public static class AutomapperConfig
    {
        public static Assembly GetAssembly()
        {
            return typeof(AutomapperConfig).Assembly;
        }

        public static IEnumerable<Profile> GetProfiles()
        {
            yield return new CustomerProfile();
            yield return new PreferenceProfile();
            yield return new PromoCodeProfile();
        }
    }
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            CreateMap<Customer, CustomerDto>();
            CreateMap<CustomerDto, Customer>();
        }
    }
    public class PreferenceProfile : Profile
    {
        public PreferenceProfile()
        {
            CreateMap<Preference, PreferenceDto>();
            CreateMap<PreferenceDto, Preference>();
        }
    }
    public class PromoCodeProfile : Profile
    {
        public PromoCodeProfile()
        {
            CreateMap<PromoCode, PromoCodeDto>()
                .ForMember(x => x.BeginDate, opt => opt.MapFrom(src => src.BeginDate.ToString()))
                .ForMember(x => x.EndDate, opt => opt.MapFrom(src => src.EndDate.ToString()));
            CreateMap<PromoCodeDto, PromoCode>()
                .ForMember(x => x.BeginDate, opt => opt.MapFrom(src => DateTime.Parse(src.BeginDate)))
                .ForMember(x => x.EndDate, opt => opt.MapFrom(src => DateTime.Parse(src.EndDate)));
        }
    }
}
