﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.BaseDto
{
    public class CustomerDto
    {
        public long Id { get; set; }

        [MaxLength(255, ErrorMessage = "Максимальная длина имени равна 255 символам")]
        public string FirstName { get; set; }
        [MaxLength(255, ErrorMessage = "Максимальная длина фамилии равна 255 символам")]
        public string LastName { get; set; }

        public string Email { get; set; }

        public List<PreferenceDto> Preferences { get; set; }
    }
}
