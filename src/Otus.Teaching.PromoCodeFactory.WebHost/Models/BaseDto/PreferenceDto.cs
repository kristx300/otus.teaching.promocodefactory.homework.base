﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.BaseDto
{
    public class PreferenceDto
    {
        public long Id { get; set; }

        [MaxLength(255, ErrorMessage = "Максимальная длина названия равна 255 символам")]
        public string Name { get; set; }
    }
}
