﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleItemResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}