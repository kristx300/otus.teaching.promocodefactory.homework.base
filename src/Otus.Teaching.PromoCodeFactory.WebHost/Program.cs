using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Program
    {
        public static string AppName { get; set; } = "Otus.PromoCodeFactory";

        public static int Main(string[] args)
        {
            IConfiguration configuration = GetConfiguration();

            Log.Logger = CreateSerilogLogger(configuration);

            try
            {
                IHost host = CreateHostBuilder(args).Build();
                Log.Information("Starting host...");
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly.");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }

        private static IConfiguration GetConfiguration()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        private static ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            string seqServerUrl = configuration["Serilog:SeqServerUrl"];
            string logstashUrl = configuration["Serilog:LogstashgUrl"];
            LoggerConfiguration cfg = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console();
            if (!string.IsNullOrWhiteSpace(seqServerUrl))
            {
                cfg.WriteTo.Seq(seqServerUrl);
            }

            if (!string.IsNullOrWhiteSpace(logstashUrl))
            {
                cfg.WriteTo.Http(logstashUrl);
            }

            return cfg.CreateLogger();
        }
    }
}