using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Base;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappings;
using Otus.Teaching.PromoCodeFactory.WebHost.Middlewares;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;

        }
        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddControllersWithViews()
                .AddNewtonsoftJson()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                })
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            services
                .AddAutoMapper(AutomapperConfig.GetAssembly());

            services.AddDbContext<PromoCodeDbContext>(options =>
                options
                    .UseLazyLoadingProxies()
                    .UseNpgsql(Configuration.GetConnectionString("Postgre"))
                );

            services.AddScoped(typeof(IEmployeeRepository), typeof(EmployeeRepository));
            services.AddScoped(typeof(IRoleRepository), typeof(RoleRepository));
            services.AddScoped(typeof(ICustomerRepository), typeof(CustomerRepository));
            services.AddScoped(typeof(IPreferenceRepository), typeof(PreferenceRepository));
            services.AddScoped(typeof(IPromoCodeRepository), typeof(PromoCodeRepository));

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app
                //.UseMiddleware<ExceptionHandlerMiddleware>()
                .UseMiddleware<LoggingMiddleware>();

            app.UseStaticFiles();
            app.UseRouting();

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            MigrateOrFail(app);

            // Should be placed at the end of this method.
            //app.UseMiddleware<DefaultNotFoundPageMiddleware>();
        }

        public void MigrateOrFail(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetRequiredService<PromoCodeDbContext>();
            try
            {
                context.Database.Migrate();
                if (!context.Roles.Any())
                {
                    context.Roles.AddRange(FakeDataFactory.Roles);
                    context.SaveChanges();
                }

                if (!context.Employees.Any())
                {
                    context.Employees.AddRange(FakeDataFactory.Employees);
                    context.SaveChanges();
                }

                if (!context.RoleEmployee.Any())
                {
                    var e1 = context.Employees.FirstOrDefault(f => f.Id == 1);
                    var e2 = context.Employees.FirstOrDefault(f => f.Id == 2);
                    var r1 = context.Roles.FirstOrDefault(f => f.Id == 1);
                    var r2 = context.Roles.FirstOrDefault(f => f.Id == 2);
                    context.RoleEmployee.Add(new RoleEmployee(r1,e1));
                    context.RoleEmployee.Add(new RoleEmployee(r2,e2));
                    context.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw new InvalidOperationException("Cannot migrate database", exception);
            }
        }
    }
}