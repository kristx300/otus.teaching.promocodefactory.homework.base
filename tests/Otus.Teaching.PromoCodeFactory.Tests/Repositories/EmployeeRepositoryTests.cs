﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.Tests.Repositories
{
    [TestFixture]
    public class EmployeeRepositoryTests
    {

        [Test]
        public async Task GetAllAsync()
        {
            AutoMapperService.Initialize();
            await using var dbContext = new PromoCodeDbContext();
            var mappings = AutoMapperService.Mapper;
            var employeeRepository = new EmployeeRepository(dbContext);
            Assert.NotNull(employeeRepository);

            var employees = await employeeRepository.GetAllAsync();
            Assert.NotNull(employees);
            Assert.AreEqual(FakeDataFactory.Employees.Count(),employees.Count());
        }


        [Test]
        public async Task GetAllAsyncAndInsert()
        {
            AutoMapperService.Initialize();
            await using var dbContext = new PromoCodeDbContext();
            var mappings = AutoMapperService.Mapper;
            var employeeRepository = new EmployeeRepository(dbContext);
            Assert.NotNull(employeeRepository);

            var employees = await employeeRepository.GetAllAsync();
            Assert.NotNull(employees);
            Assert.AreEqual(FakeDataFactory.Employees.Count(), employees.Count());

            var id = await employeeRepository.InsertAsync(new Employee
            {
                FirstName = "Sam",
                LastName = "Smith",
                Email = "samsmith@gmail.com",
            });

            Assert.AreNotEqual(Guid.Empty,id);

            employees = await employeeRepository.GetAllAsync();
            Assert.NotNull(employees);
            Assert.AreEqual(FakeDataFactory.Employees.Count() + 1, employees.Count());
        }

        [Test]
        public async Task GetAllAsyncAndUpdate()
        {
            AutoMapperService.Initialize();
            await using var dbContext = new PromoCodeDbContext();
            var mappings = AutoMapperService.Mapper;
            var employeeRepository = new EmployeeRepository(dbContext);
            Assert.NotNull(employeeRepository);

            var employees = await employeeRepository.GetAllAsync();
            Assert.NotNull(employees);

            var enumerable = employees.ToList();
            Assert.AreEqual(FakeDataFactory.Employees.Count(), enumerable.Count());

            var target = enumerable.First();
            target.FirstName = "Johnnew";

            await employeeRepository.UpdateAsync(target);
            
            var employee = await employeeRepository.GetByIdOrNullAsync(target.Id);
            Assert.NotNull(employee);
            Assert.AreEqual("Johnnew", employee.FirstName);

            employees = await employeeRepository.GetAllAsync();
            Assert.NotNull(employees);
            Assert.AreEqual(FakeDataFactory.Employees.Count(), employees.Count());
        }

        [Test]
        public async Task GetAllAsyncAndDelete()
        {
            AutoMapperService.Initialize();
            await using var dbContext = new PromoCodeDbContext();
            var mappings = AutoMapperService.Mapper;
            var employeeRepository = new EmployeeRepository(dbContext);
            Assert.NotNull(employeeRepository);

            var employees = await employeeRepository.GetAllAsync();
            Assert.NotNull(employees);

            var enumerable = employees.ToList();
            Assert.AreEqual(FakeDataFactory.Employees.Count(), enumerable.Count());

            var target = enumerable.First().Id;

            await employeeRepository.DeleteAsync(target);

            var employee = await employeeRepository.GetByIdOrNullAsync(target);
            Assert.Null(employee);

            employees = await employeeRepository.GetAllAsync();
            Assert.NotNull(employees);
            Assert.AreEqual(FakeDataFactory.Employees.Count() - 1, employees.Count());
        }
    }
}
